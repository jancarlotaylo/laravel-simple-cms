<x-layout>
  <div class="h-56" style="background: url('{{ asset('images/logo.png') }}') no-repeat center"></div>
  <div
      class="bg-gray-50 border border-gray-200 p-10 rounded max-w-lg mx-auto"
  >
      <header class="text-center">
          <h2 class="text-2xl font-bold uppercase mb-1">
              Sign Up
          </h2>
          <p class="mb-4">Create an account to post articles</p>
      </header>

      <form method="POST" action="{{ url("/") }}/users">
        @csrf
          <div class="mb-6">
              <label for="name" class="inline-block text-lg mb-2">
                  Name
              </label>
              <input
                  type="text"
                  class="border border-gray-200 rounded p-2 w-full"
                  id="name"
                  name="name"
                  value="{{ old('name') }}"
              />

              @error('name')
                <p class="text-red-500 text-xs mt-1">{{$message}}</p>
              @enderror
          </div>

          <div class="mb-6">
              <label for="email" class="inline-block text-lg mb-2"
                  >Email</label
              >
              <input
                  type="email"
                  class="border border-gray-200 rounded p-2 w-full"
                  id="email"
                  name="email"
                  value="{{ old('email') }}"
              />

              @error('email')
                <p class="text-red-500 text-xs mt-1">{{$message}}</p>
              @enderror
          </div>

          <div class="mb-6">
              <label
                  for="password"
                  class="inline-block text-lg mb-2"
              >
                  Password
              </label>
              <input
                  type="password"
                  class="border border-gray-200 rounded p-2 w-full"
                  id="password"
                  name="password"
                  value=""
              />

              @error('password')
                <p class="text-red-500 text-xs mt-1">{{$message}}</p>
              @enderror
          </div>

          <div class="mb-6">
              <label
                  for="password_confirmation"
                  class="inline-block text-lg mb-2"
              >
                  Confirm Password
              </label>
              <input
                  type="password"
                  class="border border-gray-200 rounded p-2 w-full"
                  id="password_confirmation"
                  name="password_confirmation"
              />

              @error('password_confirmation')
                <p class="text-red-500 text-xs mt-1">{{$message}}</p>
              @enderror
          </div>

          <div class="mb-6">
              <button
                  type="submit"
                  class="bg-green-600 text-white rounded py-2 px-4 hover:bg-black"
              >
                  Sign Up
              </button>
          </div>

          <div class="mt-8">
              <p>
                  Already have an account?
                  <a href="{{ url('/') }}/users/login" class="text-green-700"
                      >Login</a
                  >
              </p>
          </div>
      </form>
  </div>
</x-layout>