
<nav class="bg-gray-200 flex justify-between items-center">
  <a href="/"
    ><img class="w-24" src="{{ asset('images/logo.png') }}" alt="" class="logo"
  /></a>
  <ul class="flex space-x-6 mr-6 text-lg">
  @auth
    <li>
      <span href="{{ url('/') }}/users/login" class="hover:text-green-700"
        >
        Welcome {{ auth()->user()->name }}</span
      >
    </li>
    <li>
      <a href="{{ url('/') }}/users/logout" class="hover:text-green-700"
        ><i class="fa-solid fa-arrow-right-to-bracket"></i>
        Logout</a
      >
    </li>
  @else
    <li>
      <a href="{{ url('/') }}/users/signup" class="hover:text-green-700"
        ><i class="fa-solid fa-user-plus"></i> Sign Up</a
      >
    </li>
    <li>
      <a href="{{ url('/') }}/users/login" class="hover:text-green-700"
        ><i class="fa-solid fa-arrow-right-to-bracket"></i>
        Login</a
      >
    </li>
  @endauth
  </ul>
</nav>