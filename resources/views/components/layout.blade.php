<!doctype html>
  <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="{{ asset('images/logo.png') }}" />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
      integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
      crossorigin="anonymous"
      referrerpolicy="no-referrer"
    />
    <script src="//unpkg.com/alpinejs" defer></script>
    <script src="https://cdn.tailwindcss.com"></script>
    <script>
      tailwind.config = {
        theme: {
          extend: {
            colors: {
              laravel: "#ef3b2d",
            },
          },
        },
      };
    </script>
    <title>{{ config('app.name', 'Laravel Simple CMS') }}</title>
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"> -->
  </head>
  <body class="mb-48">
    @if (preg_match("/cms\-admin/", request()->route()->getAction()['prefix']))
      @include('partials._admin-bar')
    @endif
    <main>
      {{ $slot }}
    </main>
    <footer
      class="fixed bottom-0 left-0 w-full flex items-center justify-start font-bold bg-blue-400 text-white h-24 mt-24 opacity-90 md:justify-center"
    >
      <p class="ml-2">Copyright &copy; {{ date("Y") }}, All Rights reserved</p>
    </footer>
    <x-flash-message />
  </body>
</html>