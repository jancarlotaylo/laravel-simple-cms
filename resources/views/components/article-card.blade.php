@props(['article'])

@php

$titleLength = 100;

$article->title_trimmed = strlen($article->title) <= $titleLength ? $article->title : substr($article->title, 0, $titleLength) . "..";

if (!empty($article->thumbnail)) {
  $thumbnail = asset('storage') . '/' . $article->thumbnail;
} else {
  $thumbnail = null;
}

$home = true;

// $location = $article->location();

// @unless ($location === false)
// <div class="text-lg mb-4">
//     <i class="fa-solid fa-location-dot"></i> &nbsp;
//     {{
//       $location->cityName . ", " .
//       $location->regionName . ", " .
//       $location->countryCode
//     }}
// </div>
// @endunless

@endphp

<x-card :background="$thumbnail" :home="$home">
  <div
    class="flex cursor-pointer" onclick="location.href='{{ url('/') }}/article/{{ $article->getTitleUrl($article->title) }}'">
    <div class="bg-white mt-80 pt-3 pl-6 pr-6 pb-0" style="height: 200px">
      <h3 class="text-2xl">
          {{ $article->title_trimmed }}
      </h3>
      <div class="text-xl mb-2">
        <span class="font-bold">{{ $article->createdBy()->name }}</span>
        <span class="opacity-50 italic">{{ $article->published_at !== null ? date('M d, Y g:i A', strtotime($article->published_at)) : ""; }}</span>
      </div>
      <p>{{ strip_tags($article->content) }}</p>
    </div>
  </div>
</x-card>