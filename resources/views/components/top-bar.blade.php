@props(['article'])
@if (
  auth()->user() !== null
  && (
    auth()->user()->id === $article->createdBy()->id
      ||
    auth()->user()->role()->name === config('custom.default_administrator_role_name')
    )
  )
<nav class="text-center bg-gray-400 p-5 text-white">
  You are viewing this as an <strong>{{ auth()->user()->role()->name }}</strong>.
  <a class="font-bold text-gray-800 hover:text-white hover:underline" href="{{ url('/') }}/cms-admin/article/{{ $article->id }}/edit">
    You may edit this here
  </a>
  or
  <a class="font-bold text-gray-800 hover:text-white hover:underline" href="{{ url('/') }}/cms-admin/articles">
    visit the main Article edit page
  </a>
</nav>
@endif