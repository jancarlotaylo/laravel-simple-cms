@props(['article'])

<ul class="flex">
  @foreach ($article->tags as $tag)
    <li
        class="bg-yellow-400 text-black rounded-xl px-3 py-1 mr-2"
    >
        <a href="/tags/{{ $tag }}">{{ $tag }}</a>
    </li>
  @endforeach
</ul>