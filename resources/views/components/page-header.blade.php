<header>
    <h2 class="text-2xl font-bold uppercase mb-8">
        {{ $slot }}
    </h2>
</header>