@props(['background', 'home'])

@php
if (!isset($home)) {
  $home = false;
}
@endphp

<div style="background: url('{{ $background ?? ($home ? "https://img.freepik.com/free-photo/light-gray-concrete-wall_53876-89532.jpg?w=1060&t=st=1701510375~exp=1701510975~hmac=cc4c00a0be9feecfd18148c768858661be0ee9b4f2ee99304225e048dfb385d7" : "") }}')" class="bg-gray-50{{ $home ? " border border-gray-200 rounded-xl" : ""}}">
  {{ $slot }}
</div>