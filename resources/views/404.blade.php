<x-layout>
  @include('partials._search')
  <a href="/" class="inline-block text-black ml-4 mb-4 mt-4"
      ><i class="fa-solid fa-arrow-left"></i> Back
  </a>
  <div class="relative sm:flex sm:justify-center sm:items-center bg-dots-darker bg-center bg-gray-100 dark:bg-dots-lighter dark:bg-gray-900 selection:bg-green-700 selection:text-white">
    <img src="{{ asset('images/404.png') }}" />
  </div>
</x-layout>