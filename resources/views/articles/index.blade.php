@php

$contentLength = 250;

foreach ($data as &$row) {
  $row->is_content_trimmed = false;

  if (strlen($row->content) > $contentLength) {
    $row->content = substr(strip_tags($row->content), 0, $contentLength) . "...";
    $row->is_content_trimmed = true;
  }
}

@endphp

<x-layout>
  <!-- Remove hero section, only for home page -->
  @if(request()->route()->getAction()['prefix'] === "/articles")
    @include('partials._hero')
  @endif
  @include('partials._search')
  <div
      class="lg:grid lg:grid-cols-2 gap-4 space-y-4 md:space-y-0 mx-4"
  >
    @unless (count($data) == 0)
      @foreach ($data as $row)
        <x-article-card :article="$row" />
      @endforeach
    @else
    <p>No articles found</p>
    @endunless
  </div>
  <div class="mt-6 p-4">
    {{ $data->links() }}
  </div>
</x-layout>