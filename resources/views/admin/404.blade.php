<x-layout>
  <a href="{{ back()->getTargetUrl() }}" class="inline-block text-black ml-4 mb-4 mt-4"
      ><i class="fa-solid fa-arrow-left"></i> Back
  </a>
  <div class="relative sm:flex sm:justify-center sm:items-center selection:text-white">
    <img src="{{ asset('images/404.png') }}" />
  </div>
</x-layout>