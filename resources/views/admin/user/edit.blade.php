<x-layout>
  <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
    tinymce.init({
      selector: 'textarea#content',
      skin: 'bootstrap',
      plugins: 'lists, link, image, media',
      toolbar: 'h1 h2 bold italic strikethrough blockquote bullist numlist backcolor | link image media | removeformat help',
      menubar: false,
    });
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.2.0/datepicker.min.js"></script>
  <div class="mx-4">
      <div
          class="bg-gray-50 border border-gray-200 p-10 rounded mx-auto mt-4"
      >
          <x-page-header>
            Edit user
          </x-page-header>
          <form method="POST" action="{{ url("/") }}/cms-admin/user/{{ $user->id }}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
              <div class="mb-6">
                  <label
                      for="name"
                      class="inline-block text-lg mb-2"
                      >Name</label
                  >
                  <input
                      type="text"
                      class="border border-gray-200 rounded p-2 w-full"
                      id="name"
                      name="name"
                      value="{{ old('name') ?? $user->name }}"
                  />

                  @error('name')
                    <p class="text-red-500 text-xs mt-1">{{$message}}</p>
                  @enderror
              </div>

              <div class="mb-6">
                  <label
                      for="email"
                      class="inline-block text-lg mb-2"
                      >Email</label
                  >
                  <input
                      type="text"
                      class="border border-gray-200 rounded p-2 w-full"
                      id="email"
                      name="email"
                      value="{{ old('email') ?? $user->email }}"
                  />

                  @error('email')
                    <p class="text-red-500 text-xs mt-1">{{$message}}</p>
                  @enderror
              </div>

              <div class="mb-6">
                  <label
                      for="role_id"
                      class="inline-block text-lg mb-2"
                  >
                      Role
                  </label>
                  <select
                      class="border border-gray-200 rounded p-2 w-full"
                      id="role_id"
                      name="role_id"
                  >
                    @foreach ($roles as $role)
                      @if (old('role_id') ?? $user->role_id == $role->id)
                          <option value="{{ $role->id }}" selected>{{ $role->name }}</option>
                      @else
                          <option value="{{ $role->id }}">{{ $role->name }}</option>
                      @endif
                    @endforeach
                  </select>

                  @error('role_id')
                    <p class="text-red-500 text-xs mt-1">{{$message}}</p>
                  @enderror
              </div>

              <div class="mb-6">
                  <button
                      class="bg-green-600 text-white rounded py-2 px-4 hover:bg-black"
                  >
                      Edit User
                  </button>
                  <a href="{{ url('/') }}/cms-admin/users/" class="text-black ml-4"> Back </a>
              </div>
          </form>
      </div>
  </div>
</x-layout>