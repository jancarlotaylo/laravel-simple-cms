<x-layout>
  <div class="mx-4">
    <div
        class="bg-gray-50 border border-gray-200 p-10 rounded mx-auto"
    >
      <x-page-header>
        Dashboard
      </x-page-header>
      <div>
        @if (auth()->user()->role()->name == config('custom.default_administrator_role_name'))
        <a href="{{ url('/') }}/cms-admin/users" class="bg-green-700 p-20 text-white border border-white rounded-xl"><i class="fa fa-user"></i> &nbsp; Users </a>
        @endif
        <a href="{{ url('/') }}/cms-admin/articles" class="bg-green-700 p-20 text-white border border-white rounded-xl"><i class="fa fa-user"></i> &nbsp; Articles </a>
      </div>
    </div>
  </div>
</x-layout>