<x-layout>
  <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
    tinymce.init({
      selector: 'textarea#content',
      skin: 'bootstrap',
      plugins: 'lists, link, image, media',
      toolbar: 'h1 h2 bold italic strikethrough blockquote bullist numlist backcolor | link image media | removeformat help',
      menubar: false,
    });
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.2.0/datepicker.min.js"></script>
  <div class="mx-4">
      <div
          class="bg-gray-50 border border-gray-200 p-10 rounded mx-auto mt-4"
      >
          <x-page-header>
            Add article
          </x-page-header>
          <form method="POST" action="{{ url("/") }}/cms-admin/article/add" enctype="multipart/form-data">
            @csrf
              <div class="mb-6">
                  <label
                      for="title"
                      class="inline-block text-lg mb-2"
                      >Title</label
                  >
                  <input
                      type="text"
                      class="border border-gray-200 rounded p-2 w-full"
                      id="title"
                      name="title"
                      value="{{ old('title') }}"
                  />

                  @error('title')
                    <p class="text-red-500 text-xs mt-1">{{$message}}</p>
                  @enderror
              </div>

              <div class="mb-6">
                  <label
                      for="content"
                      class="inline-block text-lg mb-2"
                  >
                      Content
                  </label>
                  <textarea
                      class="border border-gray-200 rounded p-2 w-full"
                      id="content"
                      name="content"
                  >{{ old('content') }}</textarea>

                  @error('content')
                    <p class="text-red-500 text-xs mt-1">{{$message}}</p>
                  @enderror
              </div>

              <div class="mb-6">
                  <label for="tags" class="inline-block text-lg mb-2">
                      Tags
                  </label>
                  <input
                      type="text"
                      class="border border-gray-200 rounded p-2 w-full"
                      id="tags"
                      name="tags[]"
                      value="{{ !empty(old('tags')) ? implode(", ", old('tags')) : "" }}"
                  />

                  @error('tags')
                    <p class="text-red-500 text-xs mt-1">{{$message}}</p>
                  @enderror
              </div>

              <div class="mb-6">
                  <label for="logo" class="inline-block text-lg mb-2">
                      Thumbnail
                  </label>
                  <input
                      type="file"
                      class="border border-gray-200 rounded p-2 w-full"
                      id="thumbnail"
                      name="thumbnail"
                  />

                  @error('thumbnail')
                    <p class="text-red-500 text-xs mt-1">{{$message}}</p>
                  @enderror
              </div>

              <div class="mb-6">
                  <label for="published_at" class="inline-block text-lg mb-2">
                      Publish Date (Optional)
                  </label>
                  <input
                    datepicker
                    type="text"
                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full ps-10 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="Select date"
                    id="published_at"
                    name="published_at"
                    value="{{ old('published_at') }}"
                  />

                  @error('published_at')
                    <p class="text-red-500 text-xs mt-1">{{$message}}</p>
                  @enderror
              </div>

              <div class="mb-6">
                  <button
                      class="bg-green-600 text-white rounded py-2 px-4 hover:bg-black"
                  >
                      Add Article
                  </button>
                  <a href="{{ url('/') }}/cms-admin/articles/" class="text-black ml-4"> Back </a>
              </div>
          </form>
      </div>
  </div>
</x-layout>