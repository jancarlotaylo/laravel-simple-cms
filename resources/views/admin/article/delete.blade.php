@extends('layout')

@section('content')

<h1>
  Delete Article
</h1>

<p>{{ $data->title }}</p>

<form method="delete" action="/cms-admin/article/{{ $data->id }}">
  <p>Are you sure to delete this article?</p>
  <p>
    <button value="1">Yes</button>
    <button value="0">No</button>
  </p>
</form>

<p>
  <a href="/cms-admin/articles/">Back</a>
</p>

@endsection