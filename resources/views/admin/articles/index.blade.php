@php

$titleLength = 50;

foreach ($data as &$article) {
  $article->title_trimmed = strlen($article->title) <= $titleLength ? $article->title : substr($article->title, 0, $titleLength) . "..";

  if (is_array($article->tags)) {
    $article->tags = implode(", ", $article->tags);
  }
}

@endphp

<x-layout>

<p class="mt-5 mb-5">
  <a
    href="/cms-admin/article/add"
    class="bg-green-600 text-white rounded py-2 px-4 hover:bg-black ml-5"
  >
      Add Article
  </a>
</p>

<table width="100%">
  <thead class="text-center bg-gray-500 text-white font-bold capitalize">
    <tr>
      <td width="10%" class="py-4">Img</td>
      <td>Title</td>
      <td>Tags</td>
      <td>Published date</td>
      <td>Published by</td>
      <td>Date Created</td>
      <td>Created by</td>
      <td>Date Updated</td>
      <td>Updated by</td>
      <td width="15%">Action</td>
    </tr>
  </thead>
  <tbody>
    @unless (count($data) == 0)
      @foreach ($data as $article)
      <tr>
        <td align="center">
          @unless(empty($article->thumbnail))
            <img {{ $article->imageResize(100) }} src="{{ asset('storage') . '/' . $article->thumbnail }}" />
          @else
          <img width="100" src="{{ asset('images/no-thumbnail.png') }}" />
          @endunless
        </td>
        <td>{{ $article->title_trimmed }}</td>
        <td>{{ $article->tags }}</td>
        <td>{{ $article->published_at }}</td>
        <td>{{ optional($article->publishedBy())->name }}</td>
        <td>{{ $article->created_at }}</td>
        <td>{{ $article->createdBy()->name }}</td>
        <td>{{ $article->updated_at }}</td>
        <td>{{ $article->updatedBy()->name }}</td>
        <td class="pl-4">
          <form id="delete-{{ $article->id }}" method="POST" action="{{ url('/') }}/cms-admin/article/{{ $article->id }}">
            @csrf
            @method('DELETE')
            <a      class="bg-green-600 text-white rounded py-2 px-4 hover:bg-black" href="{{ url('/') }}/article/{{ $article->getTitleUrl($article->title) }}"><i class="fa fa-search-plus text-white"></i></a>
            <a      class="bg-green-600 text-white rounded py-2 px-4 hover:bg-black" href="{{ url('/') }}/cms-admin/article/{{ $article->id }}/edit"><i class="fa fa-pencil text-white"></i></a>
            <a      class="bg-green-600 text-white rounded py-2 px-4 hover:bg-black cursor-pointer" onclick="document.getElementById('delete-{{ $article->id }}').submit()"><i class="fa fa-trash text-white"></i></a>
          </form>
        </td>
      </tr>
      @endforeach
    @else
    <tr>
      <td colspan="10" class="text-center py-5">No articles found</td>
    </tr>
    @endunless
  </tbody>
</table>
<div class="mt-6 p-4">
  {{ $data->links() }}
</div>
</x-layout>