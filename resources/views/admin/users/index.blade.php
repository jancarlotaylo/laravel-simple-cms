<x-layout>

<!-- <p class="mt-5 mb-5">
  <a
    href="/cms-admin/user/add"
    class="bg-green-600 text-white rounded py-2 px-4 hover:bg-black ml-5"
  >
      Add User
  </a>
</p> -->

<table width="100%">
  <thead class="text-center bg-gray-500 text-white font-bold capitalize">
    <tr>
      <td>Name</td>
      <td>Email</td>
      <td>Role</td>
      <td>Date Created</td>
      <td>Created by</td>
      <td>Date Updated</td>
      <td>Updated by</td>
      <td width="15%">Action</td>
    </tr>
  </thead>
  <tbody>
    @unless (count($data) == 0)
      @foreach ($data as $user)
      <tr>
        <td class="py-3">{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ $user->role()->name }}</td>
        <td>{{ $user->created_at }}</td>
        <td>{{ optional($user->createdBy())->name ?? 'System'}}</td>
        <td>{{ $user->updated_at }}</td>
        <td>{{ optional($user->updatedBy())->name ?? 'System'}}</td>
        <td class="pl-4">
          <form id="delete-{{ $user->id }}" method="POST" action="{{ url('/') }}/cms-admin/user/{{ $user->id }}">
            @csrf
            @method('DELETE')
            <a class="bg-green-600 text-white rounded py-2 px-4 hover:bg-black" href="{{ url('/') }}/cms-admin/user/{{ $user->id }}/edit"><i class="fa fa-pencil text-white"></i></a>
            <a class="bg-green-600 text-white rounded py-2 px-4 hover:bg-black cursor-pointer" onclick="document.getElementById('delete-{{ $user->id }}').submit()"><i class="fa fa-trash text-white"></i></a>
          </form>
        </td>
      </tr>
      @endforeach
    @else
    <tr>
      <td colspan="8" class="text-center py-5">No users found</td>
    </tr>
    @endunless
  </tbody>
</table>
<div class="mt-6 p-4">
  {{ $data->links() }}
</div>
</x-layout>