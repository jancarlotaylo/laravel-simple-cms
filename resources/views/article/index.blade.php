<x-layout>
  <x-top-bar :article="$article"></x-top-bar>
  @include('partials._search')
  <a href="/" class="inline-block text-black ml-4 mb-4"
    ><i class="fa-solid fa-arrow-left"></i> Back
  </a>
  <div class="mx-4">
    <x-card>
        <div
            class="flex flex-col"
        >
            <h3 class="text-2xl mb-2">{{ $article->title }} @unless($article->published_at !== null)<span class="text-white bg-red-500 px-4 py-1">unpublished</span>@endunless</h3>
            <div class="text-xl mb-4">
              by 
              <span class=" font-bold">{{ $article->createdBy()->name }}</span>
              @unless ($article->published_at === null)
                on 
                <span class=" italic">{{ date('M d, Y g:i A', strtotime($article->published_at)); }}</span>
              @endunless
            </div>
            @unless (empty($article->tags))
              <x-article-tag-card :article="$article" />
            @else
            <em>no tags</em>
            @endunless
            <div class="border border-gray-200 w-full mb-6 mt-6"></div>
            @unless(empty($article->thumbnail))
              <img class="mr-6 md:block h-full mb-5" {{ $article->imageResize(800) }} src="{{ asset('storage') . '/' . $article->thumbnail }}" />
            @endunless
            <div>
                <div class="text-lg space-y-6">
                  {!! $article->content !!}
                </div>
            </div>
        </div>
    </x-card>
  </div>
  <a href="/" class="inline-block text-black ml-4 mt-4"
    ><i class="fa-solid fa-arrow-left"></i> Back
  </a>
</x-layout>