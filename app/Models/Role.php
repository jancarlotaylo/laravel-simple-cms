<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Cache;

class Role extends Authenticatable
{
    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];

    public function users(): Collection
    {
        $articles = Cache::tags(['role', 'users'])->remember($this->id, config('default_cache_expiry_seconds'), function () {
            return $this->hasMany(User::class, 'id', 'role_id')
                ->select(
                'id',
                'name',
                'email'
            )->get();
        });

        return $articles;
    }
}
