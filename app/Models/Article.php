<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Stevebauman\Location\Facades\Location;
use Stevebauman\Location\Position;

class Article extends Model
{
  protected $table = 'articles';

  use SoftDeletes, HasFactory;

  protected $fillable = [
    'id',
    'title',
    'content',
    'tags',
    'thumbnail',
    'published_from',
    'published_at',
    'published_by',
    'created_at',
    'created_by',
    'updated_at',
    'updated_by',
    'deleted_at',
    'deleted_by',
  ];

  protected $casts = [
    'tags' => 'array'
  ];

  public function scopeFilter($query, array $filters)
  {
    if ($filters['tag'] ?? false) {
      $query->whereJsonContains("tags", $filters['tag']);
    }

    if ($filters['search'] ?? false) {
      $query->where(function ($q) use ($filters) {
        $q->whereRaw("title like ?", '%' . $filters['search'] . '%');
        $q->orWhereRaw("content like ?", '%' . $filters['search'] . '%');
      });
    }
  }

  public static function getByTitleUrl(string $titleUrl): ?Article
  {
    Cache::flush();
    $articles = Cache::tags(['articles'])->remember('all', config('default_cache_expiry_seconds'), function () {
      if (auth()->user() === null) {
        $articles = self::whereNotNull('published_at')
        ->where('published_at', '<=', Carbon::now()->format('Y-m-d H:i:s'))
        ->get();
      } else if (auth()->user()->role()->name !== config('custom.default_administrator_role_name')) {
        $articles = self::where('created_by', auth()->user()->id)
          ->get();
      } else {
        $articles = self::all();
      }

      return $articles;
    });


    foreach ($articles as $article) {
      if ($titleUrl === self::getTitleUrl($article['title'])) {

        return $article;
      }
    }

    return null;
  }

  public static function getTitleUrl(string $title): string
  {
    $titleUrl = str_replace(" ", "-", $title);
    $titleUrl = preg_replace("/[^a-z0-9\-]/mi", "", $titleUrl);
    $titleUrl = strtolower($titleUrl);

    return $titleUrl;
  }

  public function location(): Position|bool
  {
    $data = Cache::tags(['article', 'location'])->remember($this->published_from, config('default_cache_expiry_forever'), function () {
      return Location::get($this->published_from);
    });

    return $data;
  }

  public function publishedBy(): ?User
  {
    $user = Cache::tags(['article', 'published_by'])->remember($this->published_by, config('default_cache_expiry_seconds'), function () {
      return $this->belongsTo(User::class, 'published_by', 'id')
        ->select(
          'id',
          'name',
          'email'
      )->first();
    });

    return $user;
  }

  public function createdBy(): User
  {
    $user = Cache::tags(['article', 'created_by'])->remember($this->created_by, config('default_cache_expiry_seconds'), function () {
      return $this->belongsTo(User::class, 'created_by', 'id')
        ->select(
          'id',
          'name',
          'email'
      )->first();
    });

    return $user;
  }

  public function updatedBy(): User
  {
    $user = Cache::tags(['article', 'updated_by'])->remember($this->updated_by, config('default_cache_expiry_seconds'), function () {
      return $this->belongsTo(User::class, 'updated_by', 'id')
        ->select(
          'id',
          'name',
          'email'
      )->first();
    });

    return $user;
  }

  public function imageResize(int $size): string
  {
    $dimensions = getimagesize(asset('storage') . '/' . $this->thumbnail);

    if ($dimensions[0] > $dimensions[1]) {
      $adj = ($dimensions[1] * $size) / $dimensions[0];
      $attribute = "width={$size} height={$adj}";
    } else {
      $adj = ($dimensions[0] * $size) / $dimensions[1];
      $attribute = "width={$adj} height={$size}";
    }

    return $attribute;
  }
}