<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    protected $table = 'users';

    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'email_verified_at',
        'password',
        'remember_token',
        'role_id',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function createdArticles(): Collection
    {
        $articles = Cache::tags(['user', 'article', 'created'])->remember($this->id, config('default_cache_expiry_seconds'), function () {
            return $this->hasMany(Article::class, 'id', 'created_by')
                ->select(
                'id',
                'title',
                'content',
                'tags',
                'thumbnail',
                'published_from',
                'published_at',
                'published_by'
            )->get();
        });

        return $articles;
    }

    public function updatedArticles(): Collection
    {
        $articles = Cache::tags(['user', 'article', 'updated'])->remember($this->id, config('default_cache_expiry_seconds'), function () {
            return $this->hasMany(Article::class, 'id', 'updated_by')
                ->select(
                'id',
                'title',
                'content',
                'tags',
                'thumbnail',
                'published_from',
                'published_at',
                'published_by'
            )->get();
        });

        return $articles;
    }

    public function role(): Role
    {
      $role = Cache::tags(['user', 'role'])->remember($this->role_id, config('default_cache_expiry_seconds'), function () {
        return $this->belongsTo(Role::class, 'role_id', 'id')
          ->select(
            'name'
        )->first();
      });
  
      return $role;
    }

    public function createdBy(): ?User
    {
      $user = Cache::tags(['user', 'created_by'])->remember($this->created_by, config('default_cache_expiry_seconds'), function () {
        return $this->belongsTo(User::class, 'created_by', 'id')
          ->select(
            'id',
            'name',
            'email'
        )->first();
      });
  
      return $user;
    }

    public function updatedBy(): ?User
    {
      $user = Cache::tags(['user', 'updated_by'])->remember($this->updated_by, config('default_cache_expiry_seconds'), function () {
        return $this->belongsTo(User::class, 'updated_by', 'id')
          ->select(
            'id',
            'name',
            'email'
        )->first();
      });
  
      return $user;
    }
}
