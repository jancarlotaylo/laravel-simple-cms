<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
  public function create()
  {
    return view('users.signup');
  }

  public function store(Request $request)
  {
    $formFields = $request->validate([
      'name'           => 'required|string|min:3',
      'email'          => ['required', 'email', Rule::unique('users', 'email')],
      'password'       => 'required|confirmed|min:4',
    ]);

    $formFields['password'] = bcrypt($formFields['password']);

    $formFields = array_merge($formFields, [
      'role_id'        => Role::whereNot('name', config('custom.default_administrator_role_name'))->inRandomOrder()->first()->id,
      'created_at'     => Carbon::now(),
      'created_by'     => 1,
      'updated_at'     => Carbon::now(),
      'updated_by'     => 1,
    ]);

    $user = User::create($formFields);

    $formFields = [
      'remember_token' => Str::random(32),
      'created_by' => $user->id,
      'updated_by' => $user->id,
    ];

    $user->update($formFields);

    return redirect(url('/') . '/users/login')
            ->with('message', "Signed up successfully");
  }

  public function login()
  {
    return view('users.login');
  }

  public function authenticate(Request $request)
  {
    $formFields = $request->validate([
      'email'          => ['required', 'email'],
      'password'       => 'required',
    ]);

    if (auth()->attempt($formFields)) {
      $request->session()->regenerate();

      Cache::flush();

      return redirect(url('/') . '/cms-admin')->with('message', 'Logged in successfully');
    }

    return back()->withErrors(['email' => 'Invalid credentials'])->onlyInput('email');
  }

  public function logout(Request $request)
  {
    auth()->logout();

    $request->session()->invalidate();
    $request->session()->regenerateToken();

    Cache::flush();

    return redirect(url('/') . '/users' . '/login')->with('message', 'Logged out successfully');
  }
}