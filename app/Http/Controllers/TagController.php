<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class TagController extends Controller
{
  public function index(string $tag)
  {
    $search = request()->search ?? "";

    $articles = Cache::tags(['articles', 'latest', 'tag', 'search'])->remember($search, config('default_cache_expiry_seconds'), function () use ($search, $tag) {
      $articles = Article::latest()
      ->whereNotNull('published_at')
      ->where('published_at', '<=', Carbon::now()->format('Y-m-d H:i:s'))
      ->orderBy('published_at', 'desc');

      if ($tag) {
        $articles->filter(['tag' => $tag]);
      }

      if ($search) {
        $articles->filter(['search' => $search]);
      }

      return $articles->paginate(config('custom.default_pagination_count_low'));
    });

    return view('articles.index', [
      'data' => $articles,
    ]);
  }
}

