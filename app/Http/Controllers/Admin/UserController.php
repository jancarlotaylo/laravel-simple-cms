<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
  public function index()
  {
    if (!$this->hasAccess(auth()->user())) {
      return view('admin.404');
    }

    $page = request()->page ?? 1;

    $users = Cache::tags(['users', 'latest', 'paginate', 'admin'])->remember($page, config('default_cache_expiry_seconds'), function () {
      $users = User::latest()->whereNull('deleted_at');

      return $users->paginate(config('custom.default_pagination_count_high'));
    });

    return view('admin.users.index', [
      'data' => $users,
    ]);
  }

  public function create()
  {
    return view('admin.404');
    if (!$this->hasAccess(auth()->user())) {
      return view('admin.404');
    }

    return view('admin.user.add', [
      'roles' => Role::all(),
    ]);
  }

  public function store(Request $request)
  {
    return view('admin.404');
    if (!$this->hasAccess(auth()->user())) {
      return view('admin.404');
    }

    $formFields = $request->validate([
      'name'          => 'required|string',
      'email'         => 'required|email',
      'role_id'       => 'required|exists:roles,id',
    ]);

    $formFields = array_merge($formFields, [
      'password'          => Hash::make('password'),
      'remember_token'    => Str::random(32),
    ]);

    $formFields['created_at']     = Carbon::now();
    $formFields['created_by']     = auth()->user()->id;
    $formFields['updated_at']     = Carbon::now();
    $formFields['updated_by']     = auth()->user()->id;

    User::create($formFields);

    Cache::tags(['users', 'all'])->flush();
    Cache::tags(['users', 'latest', 'paginate', 'home'])->flush();
    Cache::tags(['users', 'latest', 'paginate', 'admin'])->flush();

    return redirect(url('/') . '/cms-admin/users')
            ->with('message', 'User added');
  }

  public function edit(User $user)
  {
    if (!$this->hasAccess(auth()->user())) {
      return view('admin.404');
    }

    return view('admin.user.edit', [
      'user' => $user,
      'roles' => Role::all(),
    ]);
  }

  public function update(User $user, Request $request)
  {
    if (!$this->hasAccess(auth()->user())) {
      return view('admin.404');
    }

    $formFields = $request->validate([
      'name'          => 'required|string',
      'email'         => 'required|email',
      'role_id'       => 'required|exists:roles,id',
    ]);

    $formFields['updated_at']     = Carbon::now();
    $formFields['updated_by']     = auth()->user()->id;

    $user->update($formFields);

    Cache::tags(['users', 'all'])->flush();
    Cache::tags(['users', 'latest', 'paginate', 'home'])->flush();
    Cache::tags(['users', 'latest', 'paginate', 'admin'])->flush();

    return redirect(url('/') . '/cms-admin/users')
            ->with('message', 'User updated successfully');
  }

  public function destroy(User $user)
  {
    if (!$this->hasAccess(auth()->user())) {
      return view('admin.404');
    }

    $formFields = [
      'deleted_at'     => Carbon::now(),
      'deleted_by'     => auth()->user()->id,
    ];

    $user->update($formFields);

    Cache::tags(['users', 'all'])->flush();
    Cache::tags(['users', 'latest', 'paginate', 'home'])->flush();
    Cache::tags(['users', 'latest', 'paginate', 'admin'])->flush();

    return redirect(url('/') . '/cms-admin/users')
            ->with('message', 'User deleted successfully');
  }

  private function hasAccess(User $user): bool
  {
    return (
      $user->role()->name === config('custom.default_administrator_role_name')
    );
  }
}