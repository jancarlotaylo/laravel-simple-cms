<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Cache;

class ArticleController extends Controller
{
  public function index()
  {
    $page = request()->page ?? 1;

    $articles = Cache::tags(['articles', 'latest', 'paginate', 'admin'])->remember($page, config('default_cache_expiry_seconds'), function () {
      $articles = Article::latest();

      if (auth()->user()->role()->name !== config('custom.default_administrator_role_name')) {
        $articles->where('created_by', auth()->user()->id);
      }

      return $articles->paginate(config('custom.default_pagination_count_high'));
    });

    return view('admin.articles.index', [
      'data' => $articles,
    ]);
  }

  public function create()
  {
    return view('admin.article.add');
  }

  public function store(Request $request)
  {
    $ipAddress = filter_var(
        $request->ip(), 
        FILTER_VALIDATE_IP, 
        FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE |  FILTER_FLAG_NO_RES_RANGE
    );

    $formFields = $request->validate([
      'title'          => 'required|string|unique:articles,title',
      'content'        => 'required|min:3',
      'tags'           => 'sometimes|array|min:0',
      'thumbnail'      => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
    ]);

    if (current($formFields['tags']) !== null) {
      $formFields['tags'] = explode(", ", current($formFields['tags']));
    }

    list($contentImage, $toDelete) = $this->getThumbnailUrlFromContent($request);
    if ($request->hasFile('thumbnail')) {
      $formFields['thumbnail'] = $request->file('thumbnail')->store('thumbnails', 'public');
    } else if ($contentImage !== null) {
      $formFields['thumbnail'] = $contentImage->store('thumbnails', 'public');
      $formFields['content']   = str_replace($toDelete, "", $formFields['content']);
    }

    if ($request->published_at !== null) {
      $formFields['published_at'] = Carbon::createFromFormat('m/d/Y', $request->published_at);
    } else {
      $formFields['published_at'] = null;
    }

    $formFields['published_from'] = $ipAddress !== false ? $ipAddress : config('custom.default_local_ip_address');
    $formFields['published_by']   = $formFields['published_at'] ? auth()->user()->id : null;
    $formFields['created_at']     = Carbon::now();
    $formFields['created_by']     = auth()->user()->id;
    $formFields['updated_at']     = Carbon::now();
    $formFields['updated_by']     = auth()->user()->id;

    Article::create($formFields);

    Cache::tags(['articles', 'all'])->flush();
    Cache::tags(['articles', 'latest', 'paginate', 'home'])->flush();
    Cache::tags(['articles', 'latest', 'paginate', 'admin'])->flush();

    return redirect(url('/') . '/cms-admin/articles')
            ->with('message', 'Article added successfully');
  }

  public function edit(Article $article)
  {
    if (!$this->hasAccess($article)) {
      return view('admin.404');
    }

    return view('admin.article.edit', [
      'article' => $article,
    ]);
  }

  public function update(Article $article, Request $request)
  {
    if (!$this->hasAccess($article)) {
      return view('admin.404');
    }

    $ipAddress = filter_var(
        $request->ip(), 
        FILTER_VALIDATE_IP, 
        FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE |  FILTER_FLAG_NO_RES_RANGE
    );

    $formFields = $request->validate([
      'title'          => 'required|string',
      'content'        => 'required|min:3',
      'tags'           => 'sometimes|array|min:0',
      'thumbnail'      => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
    ]);

    if (current($formFields['tags']) !== null) {
      $formFields['tags'] = explode(", ", current($formFields['tags']));
    }

    list($contentImage, $toDelete) = $this->getThumbnailUrlFromContent($request);
    if ($request->hasFile('thumbnail')) {
      $formFields['thumbnail'] = $request->file('thumbnail')->store('thumbnails', 'public');
    } else if ($contentImage !== null) {
      $formFields['thumbnail'] = $contentImage->store('thumbnails', 'public');
      $formFields['content']   = str_replace($toDelete, "", $formFields['content']);
    }

    if ($request->published_at !== null) {
      $formFields['published_at'] = Carbon::createFromFormat('m/d/Y', $request->published_at);
    } else {
      $formFields['published_at'] = null;
    }

    $formFields['published_from'] = $ipAddress !== false ? $ipAddress : config('custom.default_local_ip_address');
    $formFields['published_by']   = $formFields['published_at'] ? auth()->user()->id : null;
    $formFields['updated_at']     = Carbon::now();
    $formFields['updated_by']     = auth()->user()->id;

    $article->update($formFields);

    Cache::tags(['articles', 'all'])->flush();
    Cache::tags(['articles', 'latest', 'paginate', 'home'])->flush();
    Cache::tags(['articles', 'latest', 'paginate', 'admin'])->flush();

    return redirect(url('/') . '/cms-admin/articles')
            ->with('message', 'Article updated successfully');
  }

  public function destroy(Article $article)
  {
    if (!$this->hasAccess($article)) {
      return view('admin.404');
    }

    $formFields = [
      'deleted_at'     => Carbon::now(),
      'deleted_by'     => auth()->user()->id,
    ];

    $article->update($formFields);

    Cache::tags(['articles', 'all'])->flush();
    Cache::tags(['articles', 'latest', 'paginate', 'home'])->flush();
    Cache::tags(['articles', 'latest', 'paginate', 'admin'])->flush();

    return redirect(url('/') . '/cms-admin/articles')
            ->with('message', 'Article deleted successfully');
  }

  private function getThumbnailUrlFromContent(Request $request): ?array
  {
    preg_match("/<img\s[^>]*?src\s*=\s*['\"]([^'\"]*?)['\"][^>]*?>/im", $request->content, $matches);

    if (!empty($matches[1])) {
      $info = pathinfo($matches[1]);
      $contents = file_get_contents($matches[1]);
      $file = '/tmp' . '/' . $info['basename'];
      file_put_contents($file, $contents);
      $uploadedFile = new UploadedFile($file, $info['basename']);

      return [$uploadedFile, $matches[0]];
    }

    return null;
  }

  private function hasAccess(Article $article): bool
  {
    return (
      auth()->user()->id === $article->createdBy()->id || 
      auth()->user()->role()->name === config('custom.default_administrator_role_name')
    );
  }
}

