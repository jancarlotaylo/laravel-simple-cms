<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class ArticleController extends Controller
{
  public function index()
  {
    $page = request()->page ?? 1;

    if (!empty(request()->search)) {
      $search = request()->search;

      $articles = Cache::tags(['articles', 'latest', 'paginate', 'home', 'search', $search])->remember($page, config('default_cache_expiry_seconds'), function () use ($search) {

        $articles = Article::latest()
          ->whereNotNull('published_at')
          ->where('published_at', '<=', Carbon::now()->format('Y-m-d H:i:s'))
          ->orderBy('published_at', 'desc')
        ;

        if ($search) {
          $articles->filter(['search' => $search]);
        }

        return $articles->paginate(config('custom.default_pagination_count_low'));
      });
    } else {
      $articles = Cache::tags(['articles', 'latest', 'paginate', 'home'])->remember($page, config('default_cache_expiry_seconds'), function () {
        return Article::latest()
          ->whereNotNull('published_at')
          ->where('published_at', '<=', Carbon::now()->format('Y-m-d H:i:s'))
          ->orderBy('published_at', 'desc')
          ->paginate(config('custom.default_pagination_count_low'));
      });
    }

    return view('articles.index', [
      'data' => $articles
    ]);
  }

  public function show(string $title)
  {
    $article = Article::getByTitleUrl($title);

    if ($article) {
      return view('article.index', [
        'article' => Article::getByTitleUrl($title),
      ]);
    } else {
      return view('404');
    }
  }
}

