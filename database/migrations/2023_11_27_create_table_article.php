<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id('id');
            $table->string('title', 255)->unique()->index();
            $table->text('content')->nullable();
            $table->string('tags')->nullable()->default(null)->index();
            $table->text('thumbnail')->nullable()->default(null);
            $table->string('published_from', 255);
            $table->timestamp('published_at')->nullable()->default(null)->index();
            $table->unsignedInteger('published_by')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('articles', function (Blueprint $table) {
            $table->unsignedBigInteger('created_by')->after('created_at')->index();
            $table->unsignedBigInteger('updated_by')->after('updated_at')->index();
            $table->unsignedInteger('deleted_by')->nullable()->default(null)->after('deleted_at');

            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('articles');
    }
};
