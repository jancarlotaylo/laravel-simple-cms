<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id('id');
            $table->string('name', 255)->unique()->index();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('roles', function (Blueprint $table) {
            $table->unsignedBigInteger('created_by')->after('created_at')->index();
            $table->unsignedBigInteger('updated_by')->after('updated_at')->index();
            $table->unsignedInteger('deleted_by')->nullable()->default(null)->after('deleted_at');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('roles');
    }
};
