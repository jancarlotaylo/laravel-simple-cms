<?php

namespace Database\Factories;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Article>
 */
class ArticleFactory extends Factory
{
    private $defaultTags = [
        "sports",
        "music",
        "arts",
        "entertainment",
        "politics",
        "lifestyle",
    ];

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $createdAt = $this->fakeDateTime();
        $createdBy = $this->fakeUser()->id;
        $publishedBy = rand(0, 1) == 1 ? $createdBy : null;

        $faker = (new \Faker\Factory())::create();
        $faker->addProvider(new \Faker\Provider\Fakenews($faker));
        $faker->addProvider(new \Faker\Provider\Fakenewssource($faker));
        $title = $faker->headline;

        return [
            'title'          => $title . " " . (Carbon::now())->getPreciseTimestamp(3),
            'content'        => fake()->paragraph(200),
            'tags'           => $this->fakeTags(),
            'published_from' => fake()->ipv4(),
            'published_at'   => $publishedBy ? $this->fakeDateTime() : null,
            'published_by'   => $publishedBy,
            'created_at'     => $createdAt,
            'created_by'     => $createdBy,
            'updated_at'     => $createdAt,
            'updated_by'     => $createdBy,
        ];
    }

    private function fakeTags(): array
    {
        $tags = [];
        $count = rand(0, count($this->defaultTags));

        for ($i = 0; $i <$count; $i++) {
            $tags[] = $this->defaultTags[rand(0, count($this->defaultTags) - 1)];
        }

        return array_values(array_unique($tags));
    }

    private function fakeUser(): ?User
    {
        $user = User::all()->random();

        return $user;
    }

    private function fakeDateTime(): string
    {
        $dateTime = fake()->dateTime(); // object

        if ($dateTime < Carbon::createFromFormat("Y-m-d", Carbon::now()->format("Y") . '-01-01')) {
            $dateTime = Carbon::now()->format("Y") . $dateTime->format("-m-d H:i:s"); // string

            if (Carbon::createFromFormat("Y-m-d H:i:s", $dateTime) > Carbon::now()->subDay()) {
                $dateTime = Carbon::now()->subDay()->format("Y-m-d H:i:s"); // string
            }
        }

        if (!is_string($dateTime)) {
            $dateTime = $dateTime->format("Y-m-d H:i:s");
        }

        return $dateTime;
    }
}
