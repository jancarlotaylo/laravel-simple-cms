<?php

namespace Database\Factories;

use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    protected static ?string $password;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $createdAt = $this->fakeDateTime();
        $createdBy = 0;

        return [
            'name'              => fake()->name(),
            'email'             => fake()->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password'          => static::$password ??= Hash::make('password'),
            'remember_token'    => Str::random(32),
            'role_id'           => $this->fakeRole()->id,
            'created_at'        => $createdAt,
            'created_by'        => $createdBy,
            'updated_at'        => $createdAt,
            'updated_by'        => $createdBy,
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     */
    public function unverified(): static
    {
        return $this->state(fn (array $attributes) => [
            'email_verified_at' => null,
        ]);
    }

    private function fakeRole(): Role
    {
        $role = Role::whereNot('name', config('custom.default_administrator_role_name'))->get()->random();

        return $role;
    }

    private function fakeDateTime(): string
    {
        $dateTime = fake()->dateTime();
        $dateTime = Carbon::now()->format("Y") . $dateTime->format("-m-d H:i:s");

        return $dateTime;
    }
}
