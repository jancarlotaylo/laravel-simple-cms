<?php

namespace Database\Seeders;

use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    public function run(): void
    {
        $seeds = [
            [
                "name" => "Administrator",
            ],
            [
                "name" => "Contributor",
            ],
            [
                "name" => "Editor",
            ],
            [
                "name" => "Moderator",
            ],
        ];

        foreach ($seeds as $seed) {
            Role::create(array_merge($seed, [
                "created_at" => Carbon::now(),
                "created_by" => 1,
                "updated_at" => Carbon::now(),
                "updated_by" => 1,
            ]));
        }
    }
}
