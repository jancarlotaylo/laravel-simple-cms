<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $roleSeeder = new RoleSeeder();
        $roleSeeder->run();

        User::factory()->create([
            'name'              => 'Super Admin',
            'email'             => 'admin@test.com',
            'email_verified_at' => Carbon::now(),
            'password'          => Hash::make('1234'),
            'remember_token'    => Str::random(32),
            'role_id'           => Role::where('name', config('custom.default_administrator_role_name'))->first()->id,
            'created_at'        => Carbon::now(),
            'created_by'        => 1,
            'updated_at'        => Carbon::now(),
            'updated_by'        => 1,
        ]);

        User::factory()->create([
            'name'              => 'User Contributor',
            'email'             => 'user@test.com',
            'email_verified_at' => Carbon::now(),
            'password'          => Hash::make('1234'),
            'remember_token'    => Str::random(32),
            'role_id'           => Role::whereNot('name', config('custom.default_administrator_role_name'))->first()->id,
            'created_at'        => Carbon::now(),
            'created_by'        => 1,
            'updated_at'        => Carbon::now(),
            'updated_by'        => 1,
        ]);

        User::factory(10)->create();
        Article::factory(100)->create();

        Cache::flush();
    }
}
