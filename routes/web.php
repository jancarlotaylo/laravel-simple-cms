<?php

use App\Http\Controllers\Admin\ArticleController as AdminArticleController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\UserController as AdminUserController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\TagController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect('/articles');
});

Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'cms-admin'], function () {
        Route::group(['prefix' => 'articles'], function () {
            Route::get('/', [AdminArticleController::class, 'index']);
        });

        Route::group(['prefix' => 'article'], function () {
            Route::get('/add', [AdminArticleController::class, 'create']);
            Route::post('/add', [AdminArticleController::class, 'store']);
            Route::get('/{article}/edit', [AdminArticleController::class, 'edit']);
            Route::put('/{article}', [AdminArticleController::class, 'update']);
            Route::delete('/{article}', [AdminArticleController::class, 'destroy']);
        });

        Route::group(['prefix' => 'users'], function () {
            Route::get('/', [AdminUserController::class, 'index']);
        });

        Route::group(['prefix' => 'user'], function () {
            Route::get('/add', [AdminUserController::class, 'create']);
            Route::post('/add', [AdminUserController::class, 'store']);
            Route::get('/{user}/edit', [AdminUserController::class, 'edit']);
            Route::put('/{user}', [AdminUserController::class, 'update']);
            Route::delete('/{user}', [AdminUserController::class, 'destroy']);
        });

        Route::get('/', [DashboardController::class, 'index']);
    });
});

Route::group(['prefix' => 'articles'], function () {
    Route::get('/', [ArticleController::class, 'index']);
});

Route::group(['prefix' => 'article'], function () {
    Route::get('/{title}', [ArticleController::class, 'show']);
});

Route::group(['prefix' => 'tags'], function () {
    Route::get('/{tag}', [TagController::class, 'index']);
});

Route::group(['prefix' => 'users'], function () {
    Route::group(['middleware' => 'guest'], function () {
        Route::get('/signup', [UserController::class, 'create']);
        Route::post('/', [UserController::class, 'store']);
        Route::get('/login', [UserController::class, 'login'])->name('login');
        Route::post('/login', [UserController::class, 'authenticate']);
    });
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/logout', [UserController::class, 'logout']);
    });
});

