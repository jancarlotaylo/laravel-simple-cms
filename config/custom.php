<?php

return [
  'default_pagination_count_low' => env('DEFAULT_PAGINATION_COUNT_LOW', 3),
  'default_pagination_count_mid' => env('DEFAULT_PAGINATION_COUNT_MID', 5),
  'default_pagination_count_high' => env('DEFAULT_PAGINATION_COUNT_HIGH', 10),
  'default_local_ip_address' => env('DEFAULT_LOCAL_IP_ADDRESS', "180.190.112.183"),
  'default_cache_expiry_seconds' => env('DEFAULT_CACHE_EXPIRY_SECONDS', 300),
  'default_cache_expiry_forever' => env('DEFAULT_CACHE_EXPIRY_FOREVER', 300),
  'default_administrator_role_name' => env('DEFAULT_ADMINISTRATOR_ROLE_NAME', 'Administrator'),
];

