## Laravel Simple CMS by JC Taylo

Content management system using Laravel.

## Pre-requisites

- Laravel Homestead **[(Installation)](https://laravel.com/docs/10.x/homestead/)**

Laravel homestead instance directory should be like this

```bash
total 196
drwxrwxr-x 13 jc jc  4096 Jul 24 12:01 ./
drwxrwxr-x  6 jc jc  4096 Nov 17 08:25 ../
drwxrwxr-x  2 jc jc  4096 Feb  7  2023 art/
drwxrwxr-x  2 jc jc  4096 Feb  7  2023 .backup/
drwxrwxr-x  2 jc jc  4096 Feb  7  2023 bin/
-rw-rw-r--  1 jc jc   187 Feb  7  2023 CHANGELOG.md
drwxrwxr-x 16 jc jc  4096 Dec  4 08:35 code/
-rw-rw-r--  1 jc jc   864 Feb  7  2023 composer.json
-rw-rw-r--  1 jc jc 91716 Feb  7  2023 composer.lock
-rw-rw-r--  1 jc jc   213 Feb  7  2023 .editorconfig
drwxrwxr-x  8 jc jc  4096 Nov 27 13:23 .git/
-rw-rw-r--  1 jc jc    14 Feb  7  2023 .gitattributes
drwxrwxr-x  3 jc jc  4096 Feb  7  2023 .github/
-rw-rw-r--  1 jc jc   199 Feb  7  2023 .gitignore
-rw-rw-r--  1 jc jc  1675 Nov 27 16:15 Homestead.yaml
-rw-rw-r--  1 jc jc   747 Feb  7  2023 Homestead.yaml.example
-rw-rw-r--  1 jc jc   265 Feb  7  2023 init.bat
-rw-rw-r--  1 jc jc   250 Feb  7  2023 init.sh
-rw-rw-r--  1 jc jc  1077 Feb  7  2023 LICENSE.txt
-rw-rw-r--  1 jc jc   483 Feb  7  2023 phpunit.xml.dist
-rw-rw-r--  1 jc jc  2183 Feb  7  2023 readme.md
drwxrwxr-x  3 jc jc  4096 Feb  7  2023 resources/
drwxrwxr-x  4 jc jc  4096 Feb  7  2023 scripts/
drwxrwxr-x  4 jc jc  4096 Feb  7  2023 src/
drwxrwxr-x  4 jc jc  4096 Feb  7  2023 tests/
drwxrwxr-x  4 jc jc  4096 Feb  7  2023 .vagrant/
-rw-rw-r--  1 jc jc  2302 Feb  7  2023 Vagrantfile
```

## Installation

Edit `Homestead.yaml` with these configurations:

```yaml
ip: "192.168.56.56"
memory: 2048
cpus: 2
provider: virtualbox

authorize: ~/.ssh/id_rsa.pub

keys:
    - ~/.ssh/id_rsa

folders:
    - map: ~/<path_to_homestead>/code
      to: /home/vagrant/code

sites:
      php: "8.1"
    - map:  laravel.simple.cms
      to:   /home/vagrant/code/laravel-simple-cms/public
      php: "8.1"

databases:
    - homestead
    - laravel_simple_cms

features:
    - mysql: true
    - mariadb: false
    - postgresql: false
    - ohmyzsh: false
    - webdriver: false

services:
    - enabled:
          - "mysql"
```

Edit local hosts file `/etc/hosts`

```bash
192.168.56.56   laravel.simple.cms
```

Start Homestead<br />
_(This takes a while)_

```bash
vagrant up
```

Access Homestead

```bash
vagrant ssh
```

Clone the project

```bash
vagrant@homestead:~$ cd code
```

```bash
vagrant@homestead:~/code$ git clone git@gitlab.com:jancarlotaylo/laravel-simple-cms.git
Cloning into 'laravel-simple-cms'...
remote: Enumerating objects: 289, done.
remote: Counting objects: 100% (289/289), done.
remote: Compressing objects: 100% (252/252), done.
remote: Total 289 (delta 83), reused 81 (delta 6), pack-reused 0
Receiving objects: 100% (289/289), 533.06 KiB | 925.00 KiB/s, done.
Resolving deltas: 100% (83/83), done.
```

Install composer

```bash
vagrant@homestead:~/code$ cd laravel-simple-cms
```

```bash
vagrant@homestead:~/code/laravel-simple-cms$ git checkout master
```

```bash
vagrant@homestead:~/code/laravel-simple-cms$ composer install
```

Copy and modify ENV file`

```
vagrant@homestead:~/code/laravel-simple-cms$ cp .env.example .env
```

Edit .env file

Database connection set, I used the default for Homestead

```
DB_USERNAME=homestead
DB_PASSWORD=secret
```

Cache configurations

```
CACHE_DRIVER=array
```

Generate application key

```bash
vagrant@homestead:~/code/laravel-simple-cms$ php artisan key:generate
```

Run migration

```bash
php artisan migrate --seed
```

Link `storage` directory`

```
vagrant@homestead:~/code/laravel-simple-cms$ php artisan storage:link

   INFO  The [public/storage] link has been connected to [storage/app/public].  

vagrant@homestead:~/code/laravel-simple-cms$
```

Open the application

http://laravel.simple.cms

# Screenshots

Guest (Home page, view article, search)

![image](https://i.ibb.co/qNXzRBD/image-50.png){width=25%}
![image](https://i.ibb.co/x5dXCyh/image-51.png){width=25%}
![image](https://i.ibb.co/pbhCzXQ/image-52.png){width=25%}

Signup & Login

![image](https://i.ibb.co/ZLHJ8G6/image-53.png){width=25%}
![image](https://i.ibb.co/6rvHDZh/image-54.png){width=25%}

Admin/User Dashboard (Articles, users)
_A regular user can only view his/her articles_

![image](https://i.ibb.co/YTD9sTX/image-55.png){width=25%}
![image](https://i.ibb.co/WVPW2dX/image-56.png){width=25%}

Dashboard add, edit, delete, article

![image](https://i.ibb.co/r0RV30c/image-58.png){width=25%}
![image](https://i.ibb.co/897HqmD/image-60.png){width=25%}
![image](https://i.ibb.co/0ySBwNv/image-61.png){width=25%}
![image](https://i.ibb.co/sjVJJry/image-62.png){width=25%}

Deleted page and unpublished page (from guest, user, admin view)

![image](https://i.ibb.co/LYt8fh8/image-63.png){width=25%}
![image](https://i.ibb.co/9TbmdQT/image-67.png){width=25%}

Admin Dashboard

![image](https://i.ibb.co/vkTzJGj/image-64.png){width=25%}
![image](https://i.ibb.co/XkXbWZz/image-65.png){width=25%}
![image](https://i.ibb.co/stvqvf7/image-59.png){width=25%}

## Features

- Content tagging
- Caching using Laravel Cache
- Access control (admin, users, guest)
